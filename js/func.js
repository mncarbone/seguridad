const SERVICE = 'https://mnc-app.firebaseio.com';
const DATABASE = 'pruebas';
const IDKEY = 'orderBy="$key"&equalTo';

function ajaxPost(url, object, callback, failure){
    xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.addEventListener('error', (e)=>{
        console.log('Request failed.  Returned status of ' + xhr.status);
        if(failure){
            failure(JSON.parse('{"error":{"message":"NOT_LOADED"}}'));
        }
    });
    xhr.onload = function() {
        if (xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
        else if (xhr.status !== 200) {
            console.log('Request failed.  Returned status of ' + xhr.status);
            if(failure){
                failure(JSON.parse(xhr.responseText));
            }
        }
    };
    xhr.send(JSON.stringify(object));
}

function ajaxGet(url, callback, failure){
    fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(myJson) {
        callback(myJson);
      })
      .catch(failure);
}

function ajaxPut(url, object, callback){
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', url);
    xhr.onload = function() {
        if (xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
    };
    xhr.send(JSON.stringify(object));
}

function ajaxDelete(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', url);
    xhr.onload = function() {
        if (xhr.status === 200) {
            callback();
        }
    };
    xhr.send();
}

function saveObject(object, collection, id, callback, failure){
    if(id){
        var url = `${SERVICE}/${DATABASE}/${collection}/${id}.json`;
        ajaxPut(url, object, callback, failure);
    }
    else{
        var url = `${SERVICE}/${DATABASE}/${collection}.json`;
        ajaxPost(url, object, callback, failure);
    }
}

function deleteObject(collection, id, callback){
    var url = `${SERVICE}/${DATABASE}/${collection}/${id}.json`;
    ajaxDelete(url, function(responseObject){
        callback(responseObject);
    });
}

function readObject(collection, id, callback){
    var url = `${SERVICE}/${DATABASE}/${collection}/${id}.json`;
    ajaxGet(url, function(responseObject){
        callback(responseObject, id);
    }, function(){
        console.log('ERROR....');
        savedObject = JSON.parse(localStorage.getItem(collection));
        callback(savedObject[id]);
    });

}

function readObjects(collection, filter, callback){
    var url = `${SERVICE}/${DATABASE}/${collection}.json`;
    ajaxGet(url, function(responseObject){
        localStorage.setItem(collection, JSON.stringify(responseObject));
        callback(responseObject);
    }, function(){
        savedObject = JSON.parse(localStorage.getItem(collection));
        callback(savedObject);
    });
}

function getFormData(form){
    var formData = new FormData(form);
    var object = {};
    formData.forEach((value, key) => {
        if(!object[key]){
            object[key] = value;
        }
        else{
            if(!object[key].push){
                object[key] = [object[key]];
            }
            object[key].push(value);
        }

    });
    return object;
}

function setElementValue(element, value, name) {
    var type = element.type || element.tagName;
    if (type == null)
        return;
    type = type.toLowerCase();
    switch (type) {
        /*
            form
            input
                color
                date
                datetime-local
                email
                file
                image
                month

                password
                range
                reset
                search
                tel
                time
                url
                week

            label
            fieldset
            legend
            optgroup
            option
            datalist
            output

         * */

        case 'radio':
            if (value.toString().toLowerCase() == element.value.toLowerCase())
                element.checked = true;
            break;
        case 'checkbox':
            if (value)
                element.checked = (value.includes)? value.includes(element.value): true;
            break;
        case 'select-multiple':
            var values = value.constructor == Array ? value : [value];
            for (var i = 0; i < element.options.length; i++) {
                for (var j = 0; j < values.length; j++) {
                    element.options[i].selected |= element.options[i].value == values[j];
                }
            }
            break;
        case 'select':
        case 'select-one':
        case 'text':
        case 'number':
        case 'hidden':
        case 'date':
            element.value = value;
            break;
        case 'textarea':
        case 'submit':
        case 'button':
        default:
            try {
                element.innerHTML = value;
            } catch (exc) { }
    }
}

function loadFormData(form, object, id){
    form.dataset.objectId = id;
    var elements = form.elements;
    for (i=0; i<elements.length; i++){
        var element = elements[i];
        var value = (element.name)? object[element.name] : null;
        if(value){
            setElementValue(element, value);
        }
    }
    if(id){
        deleteButton = form.querySelector('[type=submit][name=delete]');
        if(deleteButton){
            deleteButton.disabled = false;
        }
    }
}

function deleteForm(form, collection,callback){
    var objectId = form.dataset.objectId;
    deleteObject(collection, objectId, function(responseObject){
        delete form.dataset.objectId;
        form.reset();
        callback(responseObject, objectId);
    });
}

function saveForm(form, collection, callback, failure){
    var dataObject = getFormData(form);
    var objectId = form.dataset.objectId;
    saveObject(dataObject, collection, objectId, function(responseObject){
        let id = (objectId)? objectId : responseObject.name
        callback(responseObject, id);
    }, failure);
}

function persistForm(form, callback, failure){
    form.addEventListener("submit", function(event){
        let collection = form.getAttribute("action");
        saveForm(form, collection, callback, failure);
        event.stopPropagation();
        event.preventDefault();
    }, false);
    var deleteButton = form.querySelector('[type=submit][name=delete]');
    if(deleteButton){
        deleteButton.addEventListener('click', function(event){
            deleteForm(form, collection, callback, failure);
            event.preventDefault();
        }, false);
        if(!form.dataset.objectId){
            deleteButton.disabled = true;
        }
    }
}

function loadForm(form, collection, id){
    readObject(collection, id, function(responseObject, id){
        loadFormData(form, responseObject, id);
    });
}

function renderTemplate(str, obj, objKey){
    //Solution #1: RegExp
    return str.replace(/\${(\w*)}/g, (orig, key) => obj.hasOwnProperty( key ) ? obj[ key ] : ((objKey && key === objKey)? obj : orig));

    //Solution #2: eval
    //~ with(obj) return eval('`'+str+'`');

    //Solution #3: Function
    //~ return new Function(...Object.keys(obj), `return \`${str}\`;`)(...Object.values(obj));  
}

function addItem(list, object, id){
    let template = document.createElement('template');
    let html = list.querySelector('template.item').innerHTML.trim();
    template.innerHTML = renderTemplate(html, object, 'item');
    let node = template.content.firstChild;
    while (node) {
        if(node.dataset) node.dataset.objectId = id;
        let nextNode = node.nextSibling;
        list.appendChild(node)
        node = nextNode;
    }
}

function clearList(list){
    let node = list.firstChild;
    while (node) {
        let nextNode = node.nextSibling;
        if(node.tagName !== 'TEMPLATE'){
            list.removeChild(node);
        }
        node = nextNode;
    }
}

function loadListData(list, objects, sortAttr, sortOrder='asc'){
    clearList(list);
    showDefaultItem(list);
    if(objects){
        if(typeof objects === 'object'){
            let keys = Object.keys(objects);
            if(sortAttr){
                keys.sort((a,b)=>{
                    let aValue = objects[a][sortAttr];
                    let bValue = objects[b][sortAttr];
                    let ord = (sortOrder && sortOrder.toLowerCase() == 'desc')? -1 : 1;
                    return (aValue > bValue)? ord : ((aValue < bValue)? (-ord) : 0);
                })
            }
            keys.forEach(function (key) {
                var object = objects[key];
                addItem(list, object, key);
            });
        }
        else{
            addItem(list, objects, 0);
        }
    }
}

function loadListArray(list, objects){
    clearList(list);
    showDefaultItem(list);
    objects.forEach(function (object, index) {
        addItem(list, object, index);
    });
}

function showLoading(list){
    if(list.querySelector('template.loading')){
        clearList(list);
        var template = document.createElement('template');
        template.innerHTML = list.querySelector('template.loading').innerHTML.trim();
        var item = template.content.firstChild;
        list.appendChild(item);
    }
}

function showEmpty(list){
    clearList(list);
    if(list.querySelector('template.empty')){
        var template = document.createElement('template');
        template.innerHTML = list.querySelector('template.empty').innerHTML.trim();
        var item = template.content.firstChild;
        list.appendChild(item);
    }
}

function showDefaultItem(list){
    if(list.querySelector('template.default')){
        var template = document.createElement('template');
        template.innerHTML = list.querySelector('template.default').innerHTML.trim();
        var item = template.content.firstChild;
        list.appendChild(item);
    }
}

function loadList(list, collection){
    showLoading(list);
    readObjects(collection, null, function(responseObject){
        if(responseObject && (Object.keys(responseObject).length > 0 || responseObject.length > 0)){
            [sortAttr, sortOrder] = (list.dataset.sort)? list.dataset.sort.split(' ') : [];
            loadListData(list, responseObject, sortAttr, sortOrder);
        }
        else{
            showEmpty(list)
        }
    });
}

function addHoldEventListener(el, callback, time=1000){
    el.addEventListener('touchstart', (event)=>{
        el.dataset.holding = setTimeout((el)=>{
            callback(event);
        }, time, el);
    }, false);

    el.addEventListener('touchend', (event)=>{
        clearTimeout(el.dataset.holding);
        delete el.dataset.holding;
    }, false);
}

function listenChanges(collection, messageListener){
    var url = `${SERVICE}/${DATABASE}/${collection}.json`;

    var source = new EventSource(url);

    source.onmessage = messageListener;

    source.addEventListener("message", function (e) {
        console.log("Message received: " + e.data);
    }, false);

    source.addEventListener("open", function (e) {
        console.log("Connection was opened.");
    }, false);

    source.addEventListener("error", function (e) {
        console.log("Error - connection was lost.");
    }, false);

    //magic goes here!
    source.addEventListener("patch", function (e) {
        console.log("Patch UP - " + e.data);
    }, false);
    //And here!
    source.addEventListener("put", function (e) {
        console.log("Put UP - " + e.data);
    }, false)
}

var autoExpand = function (field, bySize=false) {
    if(bySize){
        field.style.height = 'inherit';
        var computed = window.getComputedStyle(field);
        var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
                     + parseInt(computed.getPropertyValue('padding-top'), 10)
                     + field.scrollHeight
                     + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                     + parseInt(computed.getPropertyValue('border-bottom-width'), 10);
        field.style.height = height + 'px';
    }
    else{
        field.rows = field.value.split(/\r\n|\r|\n/).length;
    }
};

//navigator notifications polyfill
if(!navigator.notification){
    navigator.notification = {
        alert: (message, ..._)=>alert(message),
        confirm: (message, ..._)=>confirm(message),
        prompt: (message, ..._)=>prompt(message),
        beep: (..._)=>alert('Beep!')
    }
}
