app.start(()=>{

    //Load default data into some lists -----------------------------
    var data = {
        "desvios": [
            {"desvio": "Destruir Activos"},
            {"desvio": "Desordenar o Ensuciar"},
            {"desvio": "Desplazarse Rápidamente"},
            {"desvio": "Desperdiciar Recursos"},
            {"desvio": "Trabajar Sin Autorización"},
            {"desvio": "Hacer Bromas"},
            {"desvio": "No Desenergizar la Instalacion"},
            {"desvio": "Bloquear la Circulación"},
            {"desvio": "Bloquear Equipo de Emergencia"},
            {"desvio": "Colocarse en Área de Riesgo"},
            {"desvio": "Postura Corporal Inadecuada"},
            {"desvio": "Actuar Distraído"},
            {"desvio": "No Utilizar Pasamanos"},
            {"desvio": "No Respetar Ailsamiento del Area"},
            {"desvio": "No Respetar Señalización"},
            {"desvio": "Uso Incorrecto de Elemento de Protección Personal"},
            {"desvio": "Uso Incorrecto de Herramienta"},
            {"desvio": "Usar Bijouterie Inadecuada"},
            {"desvio": "Otro"}
        ],
        "motivos": [
            {"motivo": "Comunucó sin Retorno"},
            {"motivo": "Cuesta Mucho"},
            {"motivo": "Cultura Local"},
            {"motivo": "Deducción"},
            {"motivo": "Desconoce Riesgo"},
            {"motivo": "Discurso vs. Práctica (conflictos)"},
            {"motivo": "Es incómodo"},
            {"motivo": "Es hábito"},
            {"motivo": "Faltan recursos"},
            {"motivo": "Indicación del supervisor"},
            {"motivo": "No tiene Tiempo"},
            {"motivo": "No recuerda entrenamiento"},
            {"motivo": "No sabe por qué"},
            {"motivo": "Requiere mucho esfuerzo"},
            {"motivo": "Resistencia al procedimiento"},
            {"motivo": "Siempre lo hizo y nada pasó"},
            {"motivo": "Todos lo hacen y nadie es castigado"}
        ],
        "elementos": [
            {"elemento":"Delantal Químico"},
            {"elemento":"Delantal Soldadura"},
            {"elemento":"Pasamontañas Electricista"},
            {"elemento":"Buzo Térmico"},
            {"elemento":"Gorra Protección Nuca"},
            {"elemento":"Bota impermeable"},
            {"elemento":"Pantalones Soldar"},
            {"elemento":"Calzado Seguridad"},
            {"elemento":"Casco"},
            {"elemento":"Faja Lumbar"},
            {"elemento":"Cinturón de Seguridad"},
            {"elemento":"Arnes de Seguridad"},
            {"elemento":"Chaleco Reflectivo"},
            {"elemento":"Chaleco Salvavidas"},
            {"elemento":"Campera Soldadura"},
            {"elemento":"Mentonera"},
            {"elemento":"Guante de Vaqueta"},
            {"elemento":"Guante Impermeable"},
            {"elemento":"Guante para Químicos"},
            {"elemento":"Guante moteado"},
            {"elemento":"Guante anticorte"},
            {"elemento":"Guante Electricista"},
            {"elemento":"Guante Pigmentado"},
            {"elemento":"Manga Soldadura"},
            {"elemento":"Manga Anticorte"},
            {"elemento":"Máscara Soldadura"},
            {"elemento":"Anteojos de Seguridad"},
            {"elemento":"Anteojos Químicos"},
            {"elemento":"Anteojos Soldadura"},
            {"elemento":"Polainas Serpientes"},
            {"elemento":"Protector Auricular 'copa'"},
            {"elemento":"Protector Auricular 'pino'"},
            {"elemento":"Protector Facial"},
            {"elemento":"Protector Respiratorio Polvo"},
            {"elemento":"Protector Respiratorio Químicos"},
            {"elemento":"Pasamontaña Monjita"},
            {"elemento":"Indumentaria Reflectiva"},
            {"elemento":"Indumentaria contra arco eléctrico"},
            {"elemento":"Indumentaria para químicos"}
        ]
    }

    document.querySelectorAll('.list.desvios').forEach((lstDesvios)=>{
        loadListArray(lstDesvios, data.desvios);
    });

    document.querySelectorAll('.list.motivos').forEach((lstMotivos)=>{
        loadListArray(lstMotivos, data.motivos);
    });

    document.querySelectorAll('select[name=motivo]').forEach((selMotivo)=>{
        loadListArray(selMotivo, data.motivos);
    });

    document.querySelectorAll('select[name=elemento]').forEach((selElemento)=>{
        loadListArray(selElemento, data.elementos);
    });
    //END Load default data into some lists -----------------------------

    //click event on item of the list "tarjetas"
    var lstTarjetas = document.getElementById('lstTarjetas');
    if(lstTarjetas){
        lstTarjetas.addEventListener('click', event=>{
            var item = event.target;
            //open page to view item
            if (item && !item.matches("button")) {
                var objectId = item.dataset.objectId;
                if(!objectId && item.tagName.toLowerCase() !== 'li'){
                    objectId = item.parentNode.dataset.objectId;
                }
                app.openPage('verTarjeta', objectId);
            }
            //open context menu
            else if(item.matches("button.icon-context-menu")){
                let bottom = item.getBoundingClientRect().bottom;
                let cm = document.querySelector('.context-menu');
                if(cm){
                    cm.style.display='block';
                    cm.style.top = bottom + 'px';
                }
            }
        }, false);
    }

    //events on items of the list "fotos"
    var lstFotos = document.querySelector('#lstFotos');
    if(lstFotos){
        //click event on delete icon of item of the list "fotos"
        lstFotos.addEventListener('click', (event)=>{
            if(event.target.matches('button.icon-delete')){
                event.target.parentNode.remove();
            }
        }, false);

        //hold event on item of the list "fotos" shows delete icon
        addHoldEventListener(lstFotos, (event)=>{
            let el = event.target;
            if(el.matches('img')){
                el.parentNode.querySelector('input[type=radio]').checked = true;
            }
        });
    }

    //click event on button "foto"
    var btnFoto = document.getElementById("btnFoto");
    if(btnFoto){
        btnFoto.addEventListener("click", event=>{
            event.preventDefault();
            //take picture and add it to the list
            app.getPhoto(imageData=>{
                var lstFotos = document.getElementById('lstFotos');
                if(lstFotos.querySelector('.empty.mask')){
                    lstFotos.querySelector('.empty.mask').remove();
                }
                addItem(lstFotos, {"foto": imageData});
            });
        }, false);
    }
});
