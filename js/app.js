var app = {
    "app": "Seguridad Industrial",
    "version": 0.1,
    "displayData": function (element){
        let object = window[element.dataset.object];
        element.innerHTML = renderTemplate(element.innerHTML, object);
    },
    "hidePage": function(pageId, isBack){
        previousPage = document.querySelector('.page.show');
        if(previousPage && previousPage.id != pageId){
            if(!isBack){
                //save previous page in history
                app.history.push({
                    "id": previousPage.id,
                    "object": previousPage.dataset.objectId
                });
            }
            previousPage.classList.remove('show');
        }        
    },
    "loadLists": function(page, objectId){
        var lists = page.querySelectorAll('.list');
        lists.forEach((list)=>{
            if(list.dataset.source){
                //load list from source attribute
                loadList(list, renderTemplate(list.dataset.source, Object.assign({}, app, {"id":objectId})));
            }
            else if(list.matches('.empty')){
                showEmpty(list);
            }
        });        
    },
    "loadForms": function(page, objectId){
        var forms = page.querySelectorAll('.form');
        forms.forEach((form)=>{

            //set action attribute from source attribute
            form.setAttribute('action', renderTemplate(form.dataset.source, Object.assign({}, app, {"id":objectId})));

            //make form persistent
            if(!form.dataset.persistent){
                form.dataset.persistent = true;
                persistForm(form, (response)=>{
                    //reset form on save
                    form.reset();
                    let message = "Los datos se guardaron correctamente";
                    navigator.notification.alert(message, null, 'Guardar');
                    //back to previous page on save
                    app.backPage();
                }, (response)=>{
                    //show error message
                    let message = response.error.message;
                    console.log(message);
                    message = (message == 'NOT_LOADED')? 'Sin Conexión' :(
                                                         'Error al Guardar');
                    navigator.notification.alert(message, null, 'Error');
                });
            }

            // load form data
            if(objectId){
                form.reset();
                loadForm(form, form.getAttribute("action"), objectId);
            }
        });
    },
    "openPage": function(pageId, objectId, isBack){        
        //hide previous page
        app.hidePage(pageId, isBack);
        
        //show next page
        nextPage = document.getElementById(pageId);
        if(nextPage){
            
            //load lists in page
            app.loadLists(nextPage, objectId);
            
            //load forms in page
            app.loadForms(nextPage, objectId);
            
            //show page
            nextPage.dataset.objectId = objectId;
            nextPage.classList.add('show');
            nextPage.scrollTop = 0;
        }
    },
    "backPage": function(){
        let visible = app.hideMenu();
        if(!visible){
            var page = app.history.pop();
            if(page){
                app.openPage(page.id, page.object, true);
            }
            else{
                navigator.app.exitApp();
            }
        }
    },
    "showMenu": function(){
        let visible = document.getElementById('chkmenu') && document.getElementById('chkmenu').checked;
        if(!visible){
            document.getElementById('chkmenu').checked = true;
        }
        return visible;
    },
    "hideMenu": function(){
        let visible = document.getElementById('chkmenu') && document.getElementById('chkmenu').checked;
        if(visible){
            document.getElementById('chkmenu').checked = false;
        }
        return visible;
    },
    "exit": function(){
        navigator.app.exitApp();
    },
    "init": function(main){
        app.history = [];
        
        //local storage variables
        app.user = localStorage.user;
        app.userName = localStorage.userName;
        app.userId = localStorage.userId;

        //swipe events for menu
        document.querySelectorAll('#menulayer').forEach(menulayer=>{
            menulayer.addEventListener('swiped-right', function(event) {
                app.showMenu();
            }, false);
        });
        document.querySelectorAll('#menulayer, #menu').forEach(menuHidder=>{
            menuHidder.addEventListener('swiped-left', event=>app.hideMenu(), false);
        });

        //back button event
        document.addEventListener("backbutton", event=>app.backPage(), false);

        //exit app buttons
        document.querySelectorAll('.exit').forEach(btn=>{
            btn.addEventListener("click", event=>app.exit(), false);            
        });

        //link buttons
        document.querySelectorAll('.link').forEach(link=>{
            link.addEventListener('click', function(event){
                //open a page
                if(link.dataset.page){
                    app.openPage(link.dataset.page);
                }
                //open a file
                if(link.dataset.file){
                    window.location = `./${link.dataset.file}.html`;
                }
                //hide menu if it is a menu item
                if(link.classList.contains('menu-item')){
                    app.hideMenu();
                }
            }, false);
        });

        //hide context menu on click anywhere except context menu icon
        document.addEventListener('click', event=>{
            if(!event.target.matches("button.icon-context-menu")){
                document.querySelectorAll('.context-menu').forEach(cm=>cm.style.display='none');
            }
        }); 
        
        //date inputs with today as default value
        document.querySelectorAll('input[type=date][value="${today}"]').forEach(input=>{
           input.valueAsDate = new Date();
           input.defaultValue = input.value;
        });
        
        //date inputs with today as max value
        document.querySelectorAll('input[type=date][max="${today}"]').forEach(input=>{
            input.max = (new Date()).toISOString().split('T')[0]
        });
        
        //inputs with user name as default value
        document.querySelectorAll('input[type=text][value="${userName}"]').forEach(input=>{
           input.value = app.userName;
           input.defaultValue = input.value;
        });
        
        //auto growing textareas
        document.querySelectorAll('textarea').forEach(textarea=>{
            textarea.addEventListener("keyup", event=>{
                if(event.target.tagName.toLowerCase() === 'textarea'){
                    autoExpand(event.target, true);
                }
            });
        });
        
        //display data in elements linked to objects
        document.querySelectorAll('[data-object]').forEach(element=>app.displayData(element));

        //show main page
        app.openPage(document.querySelector('.page.show').id);
        
        //run main function
        main();
    },
    "getPhoto": function(callback){
        if(navigator.camera && navigator.camera.getPicture){
            navigator.camera.getPicture(function (imageData) {
                callback(imageData);
            }, function (message) {
                  navigator.notification.alert(message, null, 'Aviso');
            }, {
                  quality: 20,
                  targetWidth: 300,
                  destinationType: Camera.DestinationType.DATA_URL
            });
        }
        else{
            let message = 'La cámara no está disponible';
            navigator.notification.alert(message, null, 'Aviso');
        }
    },
    "start": function(main){
        if(!!window.cordova){
            document.addEventListener("deviceready", function () {
                console.log('ready..............');
                app.init(main);
            }, false);
        }
        else{
            app.init(main);
        }
    }
}
