const AUTHSERVICE = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty';
const AUTHKEY = 'AIzaSyCOXnoKsXiRtnEZDYHJ1koy1SEC_jaNv7U';

app.start(()=>{
    
    //login form ----------------------------------------
    var frmLogin = document.querySelector('#frmLogIn');
    if(frmLogin){
        frmLogin.addEventListener('submit', (event)=>{
            event.preventDefault();
            let url = `${AUTHSERVICE}/verifyPassword?key=${AUTHKEY}`;
            ajaxPost(url, {
                "email": frmLogin.querySelector('input[name=email]').value,
                "password": frmLogin.querySelector('input[name=password]').value,
                "returnSecureToken": true
            }, (response)=>{
                console.log(response);
                localStorage.user = response.email;
                localStorage.userName = response.displayName;
                localStorage.userId = response.localId;
                localStorage.recordar = frmLogin.querySelector('input[name=recordar]').checked;
                window.location = './main.html';
            }, (response)=>{
                let message = response.error.message;
                console.log(message);
                message = (message == 'INVALID_PASSWORD')? 'Contraseña Incorrecta' :(
                          (message == 'EMAIL_NOT_FOUND')?  'Usuario No Registrado' :(
                          (message == 'NOT_LOADED')?       'Sin Conexión' :(
                                                           'Error al Ingresar')));
                navigator.notification.alert(message, null, 'Error');
                frmLogin.reset();
                frmLogin.querySelector('input[name=email]').focus();
            });
        }, false);
        localStorage.recordar = false;
    }

    //reset password ----------------------------------------
    var btnResetPassword = document.querySelector('#btnResetPassword');
    if(btnResetPassword){
        btnResetPassword.addEventListener('click', (event)=>{
            event.preventDefault();
            localStorage.recordar = false;
            let url = `${AUTHSERVICE}/getOobConfirmationCode?key=${AUTHKEY}`;
            ajaxPost(url, {
                "email": frmLogin.querySelector('input[name=email]').value,
                "requestType": "PASSWORD_RESET"
            }, (response)=>{
                console.log(response);
                let message = 'Un e-mail fué enviado a su cuenta para reestablecer su contraseña.'
                navigator.notification.alert(message, null, 'Aviso');
            }, (response)=>{
                let message = response.error.message;
                console.log(message);
                message = (message == 'EMAIL_NOT_FOUND')?  'Usuario No Registrado' : (
                          (message == 'MISSING_EMAIL')?  'Se requiere un e-mail' : (message)) ;
                navigator.notification.alert(message, null, 'Error');
            });
        }, false);
    }

    //signup form ----------------------------------------
    var frmSignUp = document.querySelector('#frmSignUp');
    if(frmSignUp){
        frmSignUp.addEventListener('submit', (event)=>{
            event.preventDefault();
            var email = frmSignUp.querySelector('input[name=email]').value;
            var password = frmSignUp.querySelector('input[name=password]').value;
            var password_confirm = frmSignUp.querySelector('input[name=password_confirm]').value;
            var name = frmSignUp.querySelector('input[name=name]').value;
            if(password == password_confirm){
                let url = `${AUTHSERVICE}/signupNewUser?key=${AUTHKEY}`;
                ajaxPost(url, {
                    "email": email,
                    "password": password,
                    "displayName": name,
                    "returnSecureToken": true
                }, (response)=>{
                    console.log(response);
                    let message = "Registro Exitoso";
                    navigator.notification.alert(message, null, 'Confirmación');
                    document.querySelector('#frmLogIn input[name=email]').value = email;
                    app.openPage('login');
                }, (response)=>{
                    let message = response.error.message;
                    console.log(message);
                    message = (message == 'TOO_MANY_ATTEMPTS_TRY_LATER')? 'Inente Más Tarde' :(
                              (message == 'EMAIL_EXISTS')?                'Usuario Ya Registrado' :(
                              (message == 'MISSING_PASSWORD')?            'Ingrese una Contraseña' :(
                                                                          'Error al Ingresar')));
                    navigator.notification.alert(message, null, 'Error');
                });
            }
            else{
                let message = "Las contraseñas no coinciden";
                navigator.notification.alert(message, null, 'Error');
            }
        }, false);
    }
});
